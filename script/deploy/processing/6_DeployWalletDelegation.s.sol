// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "../BaseDeploy.sol";
import "../../../src/Manager/ProcessingChain/ProcessingChainManager.sol";
import "../../../src/Manager/ProcessingChain/WalletDelegation.sol";
import "../../../src/Manager/AssetChain/AssetChainManager.sol";
import "../../../src/Rollup/Rollup.sol";
import "../../../src/Staking/Staking.sol";
import "../../../src/CrossChain/LayerZero/ProcessingChainLz.sol";
import "@murky/Merkle.sol";

contract DeployWalletDelegation is BaseDeploy {
    using stdJson for string;

    function run() external {
        onlyOnProcessingChain();
        string memory json = vm.readFile(processingChainContractsPath);
        ProcessingChainManager manager = ProcessingChainManager(abi.decode(json.parseRaw(".manager"), (address)));

        vm.startBroadcast(vm.envUint("PRIVATE_KEY"));
        WalletDelegation walletDelegation = new WalletDelegation(manager.participatingInterface(), address(manager));
        console.log("Deploying wallet delegation");
        console.log(address(walletDelegation));
        manager.replaceWalletDelegation(address(walletDelegation));
        vm.stopBroadcast();
    }
}
